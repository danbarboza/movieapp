import Foundation

public protocol Coordinatable: AnyObject {
    func onFinish()
}
