import Foundation

public enum DeeplinkData {
    case content(url: URL?)
}
