import Foundation

public protocol URLCoordinator: Coordinator {
    var route: URL { get set }
}
