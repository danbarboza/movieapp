import Foundation

public enum ErrorType: Error {
    case parsing
    case fetching
    case missingUrl
    case featureNotEnabled
}

public protocol Route {
    func httpMethod() -> String
    func endpoint() -> String?
    func urlRequest() -> URLRequest?
}

public protocol NetworkManager: AnyObject {
    func call(route: Route, completion: @escaping ((Result<Data, Error>) -> Void))
}

extension Route {
    public func urlRequest() -> URLRequest? {
        guard let endpoint = endpoint(), let url = URL(string: endpoint) else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod()
        return request
    }
}

