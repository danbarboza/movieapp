import Foundation

public class AppNetworkManager: NetworkManager {

    private lazy var session: URLSession = {
        URLSession(configuration: URLSession.shared.configuration)
    }()

    public func call(route: Route, completion: @escaping ((Result<Data, Error>) -> Void)) {
        if let urlRequest = route.urlRequest() {
            session.dataTask(with: urlRequest) { data, _, error in
                if let data = data {
                    completion(.success(data))
                    return
                }
                if let error = error {
                    completion(.failure(error))
                    return
                }
            }.resume()
        }
    }
}

