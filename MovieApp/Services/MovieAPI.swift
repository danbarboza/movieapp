import Foundation

public enum MovieAPI: Route {
    case fetchMovies
    
    public func httpMethod() -> String {
        return "GET"
    }
    
    public func endpoint() -> String? {
        switch self {
        case .fetchMovies:
            return "https://api.watchmode.com/v1/list-titles/?apiKey=SAHGWPUNrJsuVXevP1V1VKUHA0n0pvq7Wm3nPTG5"
        }
    }
}
