import Foundation

public protocol MoviesServiceType {
    func fetch(completion: @escaping (Result<Movies, Error>) -> Void)
}

public class MoviesService: MoviesServiceType {
    @Injected var networkManager: NetworkManager
    
    public func fetch(completion: @escaping (Result<Movies, Error>) -> Void) {
        networkManager.call(route: MovieAPI.fetchMovies) { result in
            switch result {
            case .success(let data):
                do {
                    let movies = try JSONDecoder().decode(Movies.self, from: data)
                    completion(.success(movies))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
