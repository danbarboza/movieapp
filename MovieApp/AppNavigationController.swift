import UIKit

public class AppNavigationController: UINavigationController {
    override public var childForStatusBarStyle: UIViewController? {
        self.view.backgroundColor = .white
        return topViewController
    }
}

