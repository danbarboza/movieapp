import Foundation

protocol MoviesViewModelDelegate: AnyObject {
    func didFetchData()
}

protocol MoviesViewModelType: Coordinatable {
    var moviesService: MoviesServiceType { get set }
    var coordinator: MoviesCoordinator? { get set }
    var view: MoviesViewModelDelegate? { get set }
    func fetch()
}

protocol MoviesViewModelCoordinator: Coordinatable { }

class MoviesViewModel: MoviesViewModelType {
    
    @Injected var moviesService: MoviesServiceType
    
    private var movies: Movies?
    internal weak var coordinator: MoviesCoordinator?
    internal weak var view: MoviesViewModelDelegate?
    
    func fetch() {
        moviesService.fetch { result in
            switch result {
            case .success(let movies):
                self.movies = movies
                self.view?.didFetchData()
            case .failure(let error):
                break
            }
        }
    }
    
    func getMovies() -> Movies? {
        return movies
    }
    
    func onFinish() {
        coordinator?.onFinish()
    }
}
