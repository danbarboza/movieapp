import Foundation

public struct Movies: Decodable {
    public let titles: [Title]
    public let page: Int
    public let totalResults: Int
    public let totalPages: Int
    
    enum CodingKeys: String, CodingKey {
        case titles
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        titles = try container.decode([Title].self, forKey: .titles).filter { $0.type == "movie" }
        page = try container.decode(Int.self, forKey: .page)
        totalResults = try container.decode(Int.self, forKey: .totalResults)
        totalPages = try container.decode(Int.self, forKey: .totalPages)
    }
    
}
