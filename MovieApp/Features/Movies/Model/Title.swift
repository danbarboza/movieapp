import Foundation

public struct Title: Decodable {
    public let id: Int
    public let title: String
    public let year: Int
    public let imdbId: String
    public let tmdbId: Int
    public let type: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case year
        case imdbId = "imdb_id"
        case tmdbId = "tmdb_id"
        case type
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        year = try container.decode(Int.self, forKey: .year)
        imdbId = try container.decode(String.self, forKey: .imdbId)
        tmdbId = try container.decode(Int.self, forKey: .tmdbId)
        type = try container.decode(String.self, forKey: .type)
    }
}
