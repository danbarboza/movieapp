import UIKit

class MoviesTableView: UITableView {
    var movies: [Title]?
    weak var tableViewDelegate: MoviesViewModelDelegate? 
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        allowsSelection = false
        dataSource = self
        delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MoviesTableView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let movies = movies else { return UITableViewCell() }
        let movie = movies[indexPath.row]
        let cell = MovieCell(style: .default, reuseIdentifier: movies[indexPath.row].title, movie: movie)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let moviesCount = movies?.count ?? 0
        if moviesCount == 0 {
            tableView.setEmptyView(title: "Sem videos", message: "Favor voltar mais tarde")
        } else {
            tableView.restore()
        }
        return moviesCount
    }
}

extension MoviesTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
