import UIKit

class MovieCell: UITableViewCell {
    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, movie: Title) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        fill(movie: movie)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func fill(movie: Title) {
        setPoster(for: movie.id)
        setTitle(movie)
    }

    private func setPoster(for id: Int) {
        do {
            let urlString = "https://cdn.watchmode.com/posters/0\(id)_poster_w185.jpg"
            guard let url = URL(string: urlString) else { return }
            let data = try Data(contentsOf: url)
            let image = UIImage(data: data)
            imageView?.image = image
        } catch {

        }
    }

    private func setTitle(_ movie: Title) {
        let label = textLabel ?? UILabel()
        label.text = "\(movie.title) - (\(movie.year))"
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 12)
    }
}
