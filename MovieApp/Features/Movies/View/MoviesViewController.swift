import UIKit

class MoviesViewController: UIViewController {
    
    @Injected var viewModel: MoviesViewModel
    
    var moviesView: MoviesTableView = MoviesTableView()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        viewModel.fetch()
        viewModel.view = self
        moviesView.tableViewDelegate = self

        let title = UILabel()
        title.text = "what to watch"
        title.font = UIFont(name: "AmericanTypewriter-Bold", size: 18)
        navigationItem.titleView = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = moviesView
    }
}

extension MoviesViewController: MoviesViewModelDelegate {
    func didFetchData() {
        DispatchQueue.main.async {
            self.moviesView.movies = self.viewModel.getMovies()?.titles ?? []
            self.moviesView.reloadData()
        }
    }
}
