import UIKit

final class MoviesCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var router: CoordinatorRouterType
    weak var coordinatorDelegate: CoordinatorDelegate?
    
    init(router: CoordinatorRouterType) {
        self.router = router
    }
    
    func start() {
        let mainController = loadViewController()
        router.setToRoot(module: mainController)
    }
    
    func loadViewController() -> UIViewController {
        let moviesController = MoviesViewController()
        moviesController.viewModel.coordinator = self
        return moviesController
    }
}

extension MoviesCoordinator: MoviesViewModelCoordinator {
    func onFinish() {
        coordinatorDelegate?.onCoordinatorFinish(self)
    }
}
