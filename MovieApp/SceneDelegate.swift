import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var network: NetworkManager = AppNetworkManager()
    lazy var coordinatorRouter: CoordinatorRouterType = CoordinatorRouter()
    var coordinator: Coordinator?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        registerDependencies()
        setupRootCoordinator(windowScene: windowScene)
    }

    func sceneDidDisconnect(_ scene: UIScene) {}

    func sceneDidBecomeActive(_ scene: UIScene) {}

    func sceneWillResignActive(_ scene: UIScene) {}

    func sceneWillEnterForeground(_ scene: UIScene) {}

    func sceneDidEnterBackground(_ scene: UIScene) {}

    private func setupRootCoordinator(windowScene: UIWindowScene) {
        coordinator = MoviesCoordinator(router: coordinatorRouter)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = coordinator?.router.navigationController
        window?.makeKeyAndVisible()
        coordinator?.start()
    }
    
    private func registerDependencies() {
        DependencyContainer.register(AppNetworkManager() as NetworkManager)
        DependencyContainer.register(MoviesService() as MoviesServiceType)
        DependencyContainer.register(MoviesViewModel() as MoviesViewModel)
    }
}
