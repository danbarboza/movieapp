@testable import MovieApp

class MovieViewModelMock: MoviesViewModelDelegate {
    var didCallFetchData = false

    func didFetchData() {
        didCallFetchData = true
    }
}
