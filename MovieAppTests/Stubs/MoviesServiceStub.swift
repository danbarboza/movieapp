@testable import MovieApp
import Foundation

class MoviesServiceStub: MoviesServiceType {
    func fetch(completion: @escaping (Result<Movies, Error>) -> Void) {
        do {
            let data = movieJsonString.data(using: .utf8)!
            let movies = try JSONDecoder().decode(Movies.self, from: data)
            completion(.success(movies))
        } catch {
            completion(.failure(error))
        }
    }
}
