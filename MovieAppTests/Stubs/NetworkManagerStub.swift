// swiftlint:disable force_cast
import Foundation
import MovieApp

class NetworkManagerStub: NetworkManager {
    func call(route: Route, completion: @escaping ((Result<Data, Error>) -> Void)) {
        switch route {
        case is MovieAPI: return completion(.success(movieJsonString.data(using: .utf8)!))
        default: return completion(.failure("Error!" as! Error))
        }
    }
}
