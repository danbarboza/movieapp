import XCTest
@testable import MovieApp

class MovieViewModelTests: XCTestCase {
    var sut: MoviesViewModel!
    var view: MoviesViewModelDelegate!
    
    override func setUp() {
        DependencyContainer.register(MoviesServiceStub() as MoviesServiceType)
        sut = MoviesViewModel()
        view = MovieViewModelMock()
        sut.view = view

    }
    
    func test_list_movies() {
        sut.fetch()
        XCTAssertTrue((view as! MovieViewModelMock).didCallFetchData)
    }
}
