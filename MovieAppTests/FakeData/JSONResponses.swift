import Foundation

let movieJsonString = """
{
    "titles": [
        {
            "id": 1641645,
            "title": "Asakusa Kid",
            "year": 2021,
            "imdb_id": "tt13528562",
            "tmdb_id": 768147,
            "tmdb_type": "movie",
            "type": "movie"
        },
        {
            "id": 1642252,
            "title": "Mixtape",
            "year": 2021,
            "imdb_id": "tt1587420",
            "tmdb_id": 32471,
            "tmdb_type": "movie",
            "type": "movie"
        }
],
    "page": 1,
    "total_results": 2,
    "total_pages": 1
}
"""
