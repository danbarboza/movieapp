import XCTest
@testable import MovieApp

class MovieServiceTest: XCTestCase {
    var sut: MoviesService!

    override func setUp() {
        DependencyContainer.register(NetworkManagerStub() as NetworkManager)
        sut = MoviesService()
    }

    func test_fetch() {
        let expectation = expectation(description: "fetch test")
        sut.fetch { result in
            switch result {
            case .success(let movies):
                XCTAssertNotNil(movies)
                XCTAssertEqual(movies.titles.count, 2)
                XCTAssertEqual(movies.page, 1)
                XCTAssertEqual(movies.totalPages, 1)
                XCTAssertEqual(movies.totalResults, 2)
                
                expectation.fulfill()
            case .failure:
                break
            }
        }
        wait(for: [expectation], timeout: 1.0)
    }
}
