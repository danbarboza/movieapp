# Projeto para iFood

Como novo projeto de teste criei um app em iOS que lista os filmes de uma API. Acabei não tendo muito tempo para fazer, mas estarei descrevendo todo o trabalho feito e o que poderia ser melhorado.

## Arquitetura

Implementei um arquitetura que estou acostumado a usar no meu atual trabalho que é o MVVM-C. Não tenho costume de utilizar nenhuma programação reativa então decidi usar somentes callbacks/delegates.

## Network

Para fazer os requests para busca de dados eu criei uma camada do zero utilizando somente as APIs que a Apple disponibiliza.

## UI 

Por causa do pouco tempo que tive e por ser somente uma tela com uma lista de filmes eu acabei no design. Com mais tempo eu colocaria poster para os filmes, uma descrição e incluiria uma outra tela, talvez modal, que ao selecionar um filme mostraria os detalhes do filme. Para isso iria criar tudo com ViewCode e Snapkit para facilitar o meu trabalho com Constraints.

## Pontos de melhoria

1. Criaria telas com um melhor design
2. Faria testes de UI
3. Incluiria task no makefile para realizar a automatização do setup, teste e release do projeto, ja pensando em criar uma processo de CI/CD do APP.

